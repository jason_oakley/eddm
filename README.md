# Map Marketing Tool

A modularized service layer for working with maps and USPS route data.

There are 4 main branches to this repo (described in detail below).

### Master

Production-ready code base. Until v2, this will be the Marionette port that is being used in Ultra at this time.

### Develop

Development branch where features are integrated and tested against the master branch.

### Ultra

This is the version 1.0 EDDM-only map. It runs within Marionette and as a module of Ultra.

### Service

The new-and-improved v2 initiative where the map and outlining components/services are broken apart into platform neautral service layer(s).

---

# Map Components

...list current map components so they can be broken apart later down the line...