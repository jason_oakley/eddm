var gulp    = require( 'gulp' );

var plumber     = require( 'gulp-plumber' );
var concat      = require( 'gulp-concat' );
var minify      = require( 'gulp-minify' );
var uglify      = require( 'gulp-uglify' );
var apidoc      = require( 'gulp-apidoc' )
var _           = require( 'underscore' );

var environment = 'local';
var paths       = {
    src     : './src/app/',
    dest    : './src/assets/js/',
    vendor  : './src/assets/vendor/',
    assets  : './src/assets/'
};

if ( environment === 'production' )
{
    var paths = _.extend( paths, {
        dest: './dist'
    });
}


// Set Environment
// ===========================================================
gulp.task( 'set-environment', function ( env )
    {
        environment = env;
    }
);

// API Docs
// ===========================================================
gulp.task( 'docs', function ()
    {
        apidoc.exec({
            src : 'src/app/',
            dest: 'docs/',

            debug: true
        });
    }
);
// Clean
// ===========================================================
var del = require( 'del' );

gulp.task( 'clean', function ()
    {
        return del( [ 'dist', 'src/assets/css', 'src/assets/js' ], function ( err, deletedFiles )
            {
                if ( deletedFiles )
                {
                    console.log( 'Files deleted:', deletedFiles.join( ', ' ) );
                }
            }
        );
    }
);

// Copy
// ===========================================================
gulp.task( 'copy', function ()
    {
        return gulp.src( './src/*.html' )
            .pipe( gulp.dest( 'dist' ) );
    }
);

// BrowserSync
// ===========================================================
var browserSync = require( 'browser-sync' );
var reload      = browserSync.reload;

gulp.task( 'browser-sync', function ()
    {
        browserSync({
            notify: false,
            port    : 5309,
            server: {
                baseDir : [ './examples', './src' ]
            }
        });
    }
);

// Sass
// ===========================================================
var sass        = require( 'gulp-sass' );
var sourcemaps  = require( 'gulp-sourcemaps' );

gulp.task( 'sass', function ()
    {
        gulp.src( './examples/sass/*.scss' )
            .pipe( sourcemaps.init() )
                .pipe( sass({
                    errLogToConsole: true,

                    // sync: true,
                    onSuccess: function ( result )
                    {
                        console.log( 'Compiled Sass files' );
                    },
                    onError: function ( err )
                    {
                        console.log( 'Somethingdonebrokeohno!' );
                        console.log( err );
                    }
                }))
                // .pipe( sourcemaps.write( '/' ) )
            .pipe( plumber() )
            .pipe( gulp.dest( './examples/assets/css' ) )
            .pipe( reload({
                stream: true
            }) );
    }
);

// JSHint
// ===========================================================
var jshint  = require( 'gulp-jshint' );
var stylish = require( 'jshint-stylish' );

gulp.task( 'jshint', function ()
    {
        gulp.src( './src/app/*.js' )
            .pipe( plumber() )
            .pipe( jshint() )
            .pipe( jshint.reporter( stylish) )
            .pipe( jshint.reporter( 'fail' ) );
    }
);

// Node Server
// ===========================================================
// var nodemon = require( 'gulp-nodemon' );

// gulp.task( 'server', function ()
//     {
//         nodemon({
//             script  : 'src/server.js',
//             // ext     : 'html js',
//             // ignore  : [ 'ignored.js' ]
//         })
//         .on( 'change', [ 'jshint'] )
//         .on( 'restart', function ()
//             {
//                 console.log('restarted!')
//             }
//         );
//     }
// );

// Browser Reload
// ===========================================================
gulp.task( 'bs-reload', function ()
    {
        browserSync.reload();
    }
);

// Vendor JavaScript
// ===========================================================
// gulp.task( 'vendor-js', function ()
//     {
//         stream = gulp.src([
//             paths.vendor + 'jquery/jquery.js',
//             paths.vendor + 'underscore/underscore.js',
//             paths.vendor + 'backbone/backbone.js',
//             paths.vendor + 'marionette/backbone.marionette.js'
//         ])
//         .pipe( plumber() )
//         .pipe( concat( 'vendor.js' ) )

//         if ( environment === 'production' )
//         {
//             stream.pipe( uglify() )
//         }

//         stream.pipe( gulp.dest( paths.dest ) )
//     }
// );


// Default Task
// ===========================================================
gulp.task( 'default', [ 'clean', 'sass', 'browser-sync' ], function ()
    {
        gulp.watch( 'examples/sass/**/*.scss', [ 'sass' ] );
        gulp.watch( 'src/**/*.js', [ 'jshint', browserSync.reload ] );
        gulp.watch(' examples/**/*.html', [ 'bs-reload' ] );
    }
);
